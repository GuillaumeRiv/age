package com.example.bibliothqueage;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_concession_details extends AppCompatActivity {
    //VARIABLES GLOBALES.
    private User user;
    private Book book;
    private Concession concession;
    private Intent intent;
    private TextView tvAddBarcode, tvAddTitle, tvAddAuthor, tvAddEdition, tvAddPublisher, tvAddSummary;
    private Switch switchAddAnotation;
    private EditText etAddPrice;
    private Button btnAddPicture, btnAddSell;
    private ImageView ivAddThumbnail;
    private Bitmap selectedImage;
    private DataSingleton data = DataSingleton.getInstance();
    private String currentPhotoPath;
    private File filePicture;
    private InterfaceServeur serveur;
    private Call<Integer> call3;
    private boolean pictureIsSet = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        intent = getIntent();
        concession = new Concession();
        user = data.getConnectedUser();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_concession_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        serveur = RetrofitInstance.getInstance().create(InterfaceServeur.class);

        //REÇOIS LES EXTRAS DU INTENTS.
        book = (Book) intent.getSerializableExtra("Book");


        //TROUVE LES VIEWS.
        tvAddTitle = findViewById(R.id.tvAddTitle);
        tvAddAuthor = findViewById(R.id.tvAddAuthor);
        tvAddEdition = findViewById(R.id.tvAddEdition);
        tvAddPublisher = findViewById(R.id.tvAddPublisher);
        tvAddSummary = findViewById(R.id.tvAddSummary);
        tvAddBarcode = findViewById(R.id.tvAddBarcode);
        etAddPrice = findViewById(R.id.etAddPrice);
        switchAddAnotation = findViewById(R.id.switchAddAnotation);
        btnAddPicture = findViewById(R.id.btnAddPicture);
        btnAddSell = findViewById(R.id.btnAddSell);
        ivAddThumbnail = findViewById(R.id.ivAddThumbnail);


        //SET LES INFORMATIONS DU LIVRE.
        if (book.getTitle() != "" && book.getTitle() != null) {
            tvAddTitle.setText(book.getTitle());
        } else tvAddTitle.setText("Aucun titre");

        if (book.getAuthor() != "" && book.getAuthor() != null) {
            tvAddAuthor.setText(book.getAuthor());
        } else tvAddAuthor.setText("Aucune information sur l'auteur");

        if (book.getEdition() != "" && book.getEdition() != null) {
            tvAddEdition.setText(book.getEdition());
        } else tvAddEdition.setText("Aucune information sur l'édition");

        if (book.getPublisher() != "" && book.getPublisher() != null) {
            tvAddPublisher.setText(book.getPublisher());
        } else tvAddPublisher.setText("Aucune information sur le publieur");

        if (book.getSummary() != "" && book.getSummary() != null) {
            tvAddSummary.setText(book.getSummary());
        } else tvAddSummary.setText("Aucun résumé");

        if (book.getBarcode() != "" && book.getBarcode() != null) {
            tvAddBarcode.setText(book.getBarcode());
        } else tvAddBarcode.setText("Aucun code barre");



        btnAddPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);


                if (pickPhoto.resolveActivity(getPackageManager()) != null) {
                    // on crée le fichier devant recevoir la photo
                    filePicture = null;
                    try {
                        filePicture = createFilePicture();
                    } catch (IOException ex) {
                        // gestion d'erreur eventuelle lors de la création du fichier
                    }
                    // on continue si le fichier est créé correctement
                    if (filePicture != null) {
                        Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),
                                "com.example.bibliothqueage.fileprovider",
                                filePicture);
                        pickPhoto.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(pickPhoto, 1);
                    }
                }
            }
        });

        btnAddSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //REGEX ET INSERTION DANS LA DB.
                if (!etAddPrice.getText().toString().matches("")) {


                    //INSERT LA CONCESSION DANS LA DB.
                    concession.setIdBook(book.getId());

                    if (switchAddAnotation.isChecked()) {
                        concession.setAnnotated(1);
                    } else concession.setAnnotated(0);

                    concession.setCustomerPrice(Integer.parseInt(etAddPrice.getText().toString()));
                    concession.setIdCustomer(user.getId());

                    call3 = serveur.add_concession(concession);

                    call3.enqueue(new Callback<Integer>() {
                        @Override
                        public void onResponse(Call<Integer> call, Response<Integer> response) {
                            concession.setId(response.body());
                            if (pictureIsSet) {
                                saveImg();
                            }
                        }

                        @Override
                        public void onFailure(Call<Integer> call, Throwable t) {
                            Log.e("Error", t.getMessage());
                        }
                    });

                    finish();
                }
            }
        });


    }

    //FONTION POUR LES MENUS ITEMS.
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }



        return true;
    }

    //FONCTION SERVANT À RECEVOIR LES RESULTAT D'UNE ACTIVITÉ.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    try {
                        pictureIsSet = true;
                        final Uri imageUri = data.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        this.selectedImage = selectedImage;
                        ivAddThumbnail.setImageBitmap(selectedImage);

                        ByteArrayOutputStream bos = new ByteArrayOutputStream();

                        filePicture = null;
                        try {
                            filePicture = createFilePicture();

                            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                            byte[] bitmapdata = bos.toByteArray();

                            FileOutputStream fos = new FileOutputStream(filePicture);
                            fos.write(bitmapdata);
                            fos.flush();
                            fos.close();
                        } catch (IOException ex) {

                        }

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    //CRÉER LE FILE POUR ACCUEILLIR LA PHOTO PRISE.
    private File createFilePicture() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* dossier */
        );

        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    //FONCTION SERVANT À SAUVEGARDER L'IMAGE DANS LA DB.
    public void saveImg()
    {
        //on doit indiquer le type de fichier. Pour une image mettre "image/*"
        MediaType mediaType = MediaType.parse("image/*");
        RequestBody fichier_requete = RequestBody.create(mediaType,filePicture);

        MultipartBody.Part part_fichier = MultipartBody.Part.createFormData("photo",
                filePicture.getName(),
                fichier_requete);

        InterfaceServeur serveur = RetrofitInstance.getInstance().create((InterfaceServeur.class));
        Call<ResponseBody> call = serveur.upload(concession.getId(), part_fichier);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }


}
