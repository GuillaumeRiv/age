package com.example.bibliothqueage;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Concession implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("customerPrice")
    private double customerPrice;

    @SerializedName("sellingPrice")
    private double sellingPrice;

    @SerializedName("idCustomer")
    private int idCustomer;

    @SerializedName("idBook")
    private int idBook;

    @SerializedName("bookBarcode")
    private String bookBarcode;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("annotated")
    private int annotated;

    public Concession() {

    }

    public Concession(int id, double customerPrice, double sellingPrice, int idCustomer, int idBook, String bookBarcode, String thumbnail, int annotated) {
        this.id = id;
        this.customerPrice = customerPrice;
        this.sellingPrice = sellingPrice;
        this.idCustomer = idCustomer;
        this.idBook = idBook;
        this.bookBarcode = bookBarcode;
        this.thumbnail = thumbnail;
        this.annotated = annotated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCustomerPrice() {
        return customerPrice;
    }

    public void setCustomerPrice(double customerPrice) {
        this.customerPrice = customerPrice;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(int idCustomer) {
        this.idCustomer = idCustomer;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public String getBookBarcode() {
        return bookBarcode;
    }

    public void setBookBarcode(String bookBarcode) {
        this.bookBarcode = bookBarcode;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getAnnotated() {
        return annotated;
    }

    public void setAnnotated(int annotated) {
        this.annotated = annotated;
    }
}
