package com.example.bibliothqueage;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<Book> mData;
    private static String currentFragment;

    public void setCurrentFragment(String currentFragment) {
        this.currentFragment = currentFragment;
    }

    public static String getCurrentFragment() {
       return currentFragment;
    }

    public RecyclerViewAdapter(Context mContext, List<Book> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_book, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        String imageURI = "http://206.167.140.56:8080/420617RI/Equipe_3/BiblioAGE/book_imgs/" + mData.get(position).getThumbnail();
        Picasso.with(mContext).invalidate(imageURI);
        Picasso.with(mContext).load(imageURI).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE);

        holder.tv_car_title.setText(mData.get(position).getTitle());
        Picasso.with(mContext).load(imageURI).into(holder.img_car_thumbnail);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, Book_detailsV2.class);
                intent.putExtra("bookID", mData.get(position).getId());
                intent.putExtra("fragment", currentFragment);

                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_car_title;
        ImageView img_car_thumbnail;
        CardView cardView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_car_title = itemView.findViewById(R.id.car_title_id);
            img_car_thumbnail = itemView.findViewById(R.id.car_img_id);
            cardView = itemView.findViewById(R.id.cardview_id);

        }
    }


}
