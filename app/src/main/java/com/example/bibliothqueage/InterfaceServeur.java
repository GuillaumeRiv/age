package com.example.bibliothqueage;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.internal.Internal;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface InterfaceServeur {

    @GET("get_all_book.php")
    Call<List<Book>> getBooks();

    @FormUrlEncoded
    @POST("get_book_by_barcode.php")
    Call<Book> getBookByBarcode(@Field("barcode") String Barcode);

    @FormUrlEncoded
    @POST("get_book_by_id.php")
    Call<Book> getBookByID(@Field("idBook") int idBook);

    @FormUrlEncoded
    @POST("get_concession_by_book.php")
    Call<List<Concession>> getConcessionByBook(@Field("idBook") int idBook);

    @FormUrlEncoded
    @POST("search_book.php")
    Call<List<Book>> search_book(@Field("title") String titre);

    @FormUrlEncoded
    @POST("search_book_advanced.php")
    Call<List<Book>> search_book_advanced(@Field("title") String titre);

    @FormUrlEncoded
    @POST("get_user_check_email.php")
    Call<ServerResponse> get_user_check_email(@Field("email") String email);@FormUrlEncoded

    @POST("check_password.php")
    Call<ServerResponse> check_password(
            @Field("id") int id,
            @Field("password") String password,
            @Field("newPass") String newPass);

    @FormUrlEncoded
    @POST("get_user_check.php")
    Call<User> get_user_check(
            @Field("email") String email,
            @Field("password") String password
    );

    @POST("add_user.php")
    Call<ServerResponse> add_user(@Body User user);

    @POST("update_user.php")
    Call<ServerResponse> update_user(@Body User user);

    @POST("add_book.php")
    Call<Integer> add_book(@Body Book book);

    @POST("add_concession.php")
    Call<Integer> add_concession(@Body Concession concession);

    @POST("reserved_book.php")
    Call<ServerResponse> reserved_book(
            @Field("idConcession") int id,
            @Field("idUser") int reservedBy
    );

    @Multipart
    @POST("img_upload.php")
    Call<ResponseBody> upload(
            @Part("itemId") int itemId,
            @Part MultipartBody.Part image
    );






}
