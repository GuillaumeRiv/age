package com.example.bibliothqueage;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginCrAccActivity extends AppCompatActivity {

    private String password, passwordRep;
    private Boolean passwordCheck = false, validationComplete;
    private TextInputLayout tilCode, tilPrenom, tilNom, tilEmail, tilPhone, tilPassword, tilPasswordRep, tilMatricule;
    private EditText etEmail;
    private CheckBox checkMatricule;
    private Button bt_create;
    private InterfaceServeur serveur = RetrofitInstance.getInstance().create(InterfaceServeur.class);
    private Integer matriculeExt = 0;
    public Context context = this;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_cr_acc);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bt_create = findViewById(R.id.btCrAcc);
        tilCode = findViewById(R.id.text_Code);
        tilPrenom = findViewById(R.id.text_prenom);
        tilNom = findViewById(R.id.text_nom);
        tilEmail = findViewById(R.id.text_email);
        tilPhone = findViewById(R.id.text_tel);
        tilPassword = findViewById(R.id.text_password);
        tilPasswordRep = findViewById(R.id.text_passwordRep);
        tilMatricule = findViewById(R.id.text_matricule);
        checkMatricule = findViewById(R.id.checkMatricule);
        tilMatricule.setEnabled(false);


        checkMatricule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkMatricule.isChecked()) {
                    tilMatricule.setEnabled(true);
                } else {
                    tilMatricule.setEnabled(false);

                }
            }
        });

    }


    private Boolean validateMatricule() {

        if(checkMatricule.isChecked()){
            String matriculeInput = tilMatricule.getEditText().getText().toString().trim();

            if (matriculeInput.isEmpty()) {
                tilMatricule.setError("Remplir le champ");
                return false;
            } else if (matriculeInput.length() > 7) {
                tilMatricule.setError("Matricule trop long");
                return false;
            } else {
                tilMatricule.setError("");
                return true;
            }
        }

        return true;

    }

    private boolean validatePrenom() {
        String prenomInput = tilPrenom.getEditText().getText().toString().trim();

        if (prenomInput.isEmpty()) {
            tilPrenom.setError("Remplir le champ");
            return false;
        } else if (prenomInput.length() > 30) {
            tilPrenom.setError("Prénom trop long");
            return false;
        } else {
            tilPrenom.setError("");
            return true;
        }
    }

    private boolean validateNom() {
        String nomInput = tilNom.getEditText().getText().toString().trim();

        if (nomInput.isEmpty()) {
            tilNom.setError("Remplir le champ");
            return false;
        } else if (nomInput.length() > 30) {
            tilNom.setError("Nom trop long");
            return false;
        } else {
            tilNom.setError("");
            return true;
        }
    }


    /*IMPORTANT!!!!! S'assurer que l'adresse entré n'Est pas celle du cegep*/
    private boolean validateEmail() {

        String emailInput = tilEmail.getEditText().getText().toString().trim();

        Pattern regexEmailCegepEdu = Pattern.compile("^[a-zA-Z0-9_\\.+]+@(edu)(\\.cegeptr)(\\.qc)(\\.ca)");
        Pattern regexEmailCegepProf = Pattern.compile("^[a-zA-Z0-9_\\.+]+@(cegeptr)(\\.qc)(\\.ca)");
        Matcher matcherEdu = regexEmailCegepEdu.matcher(emailInput);
        Matcher matcherProf = regexEmailCegepProf.matcher(emailInput);

        if (matcherEdu.find() | matcherProf.find()) {
            tilEmail.setError("Vous ne pouvez pas utiliser le courriel du Cégep de Trois-Rivières");
            return false;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            tilEmail.setError("Courriel invalide");
            return false;
        }

        if (emailInput.isEmpty()) {
            tilEmail.setError("Remplir le champ");
            return false;
        } else if (emailInput.length() > 40) {
            tilEmail.setError("Courriel trop long");
            return false;
        }


        tilEmail.setError("");

        return true;

    }

    private boolean validatePhone() {
        String phoneInput = tilPhone.getEditText().getText().toString().trim();

        if (phoneInput.isEmpty()) {
            tilPhone.setError("Remplir le champ");
            return false;
        } else if (phoneInput.length() > 10) {
            tilPhone.setError("Numéro trop long");
            return false;
        } else {
            tilPhone.setError("");
            return true;
        }
    }


    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$");

        return PASSWORD_PATTERN.matcher(s).matches();
    }

    private boolean validatePassword() {
        String passwordInput = tilPassword.getEditText().getText().toString().trim();

        if (!isValidPassword(passwordInput)) {
            tilPassword.setError("Mot de passe non-conforme. Doit contenir des lettres, des chiffres et au moins un caractère spécial(ex: !@#$%&*)");
            return false;
        } else {
            tilPassword.setError("");
            return true;
        }
    }


    private boolean validatePasswordRep() {

        String passwordRepInput = tilPasswordRep.getEditText().getText().toString().trim();

        if (!isValidPassword(passwordRepInput)) {
            tilPasswordRep.setError("Mot de passe non-conforme. Doit contenir des lettres, des chiffres et au moins un caractère spécial(ex: !@#$%&*)");
            return false;
        } else {
            tilPasswordRep.setError("");
            return true;
        }


    }

    private boolean validatePasswordCheck() {
        String passwordinput = tilPassword.getEditText().getText().toString().trim();
        String passwordRepinput = tilPasswordRep.getEditText().getText().toString().trim();
        passwordCheck = passwordinput.equals(passwordRepinput);

        if (passwordCheck == false) {

            tilPasswordRep.setError("Les mots de passe ne sont pas semblables");
            return false;
        } else {
            tilPassword.setError("");
            return true;
        }
    }

    public void confirmInput(View v) {
        if (validateMatricule() & validateNom() & validatePrenom() & validateEmail() & validatePhone() & validatePassword() & validatePasswordRep() & validatePasswordCheck()) {

            Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();

            Call<ServerResponse> call = serveur.get_user_check_email(tilEmail.getEditText().getText().toString());

            //On check si le email est ok
            call.enqueue(new Callback<ServerResponse>()
            {
                @Override
                public void onResponse( Call<ServerResponse> call, Response<ServerResponse> response) {
                    Boolean available;
                    try{
                        available = response.body().getSucces();
                        Log.d("Data--checkEmail", response.body().getMessage());
                    }catch (Exception e){
                        available = false;
                        Log.d("Data--checkEmail", e.getMessage());
                    }
                    if(available){

                        String codeOk = tilCode.getEditText().getText().toString();

                        /*Maintenant, on peut sortir les variable pour les envoyer a la BD*/
                        String matriculeOk, prenomOk, nomOk, emailOk, phoneOk,passwordOk;

                        matriculeOk = tilMatricule.getEditText().getText().toString();
                        prenomOk = tilPrenom.getEditText().getText().toString();
                        nomOk = tilNom.getEditText().getText().toString();
                        emailOk = tilEmail.getEditText().getText().toString();
                        phoneOk = tilPhone.getEditText().getText().toString();
                        passwordOk = tilPassword.getEditText().getText().toString();

                        User user = new User("",prenomOk,nomOk,phoneOk,emailOk,"active",passwordOk,matriculeOk);
                        add_user_bd(user);
                        finish();
                    }else {
                        Toast.makeText(context, "Un compte existe déja à cette adresse courriel", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<ServerResponse> call, java.lang.Throwable t) {
                    Log.d("Data--checkEmail", t.getMessage());
                }
            });

        }
    }

    //FONTION POUR LES MENUS ITEMS.
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent myIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(myIntent, 0);
                this.overridePendingTransition(R.anim.animation_enter,
                        R.anim.animation_leave);
                break;
        }


        return true;
    }


    public void add_user_bd(User user){

        Call<ServerResponse> call = serveur.add_user(user);
        call.enqueue(new Callback<ServerResponse>()
        {
            @Override
            public void onResponse( Call<ServerResponse> call, Response<ServerResponse> response) {
                try{
                    Log.d("Data--update_user", response.body().getMessage());
                }catch (Exception e){
                    Log.d("Data--update_user", e.getMessage());
                }
            }
            @Override
            public void onFailure(Call<ServerResponse> call, java.lang.Throwable t) {
                try{
                    Log.d("Data--update_user", t.getMessage());
                }catch (Exception e){
                    Log.d("Data--update_user", e.getMessage());
                }
            }
        });

    }
}
