package com.example.bibliothqueage;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bibliothqueage.fragments.BookFragment;
import com.example.bibliothqueage.fragments.HomeFragment;
import com.example.bibliothqueage.fragments.ReservationFragment;
import com.example.bibliothqueage.fragments.SearchBook;
import com.example.bibliothqueage.fragments.ShopFragment;
import com.google.android.material.navigation.NavigationView;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SearchBook.searchInterface, HomeFragment.homeInterface{

    //INSTANCIATIONS DES VARIABLES.
    private Context context;
    Intent intent;
    private View header;
    private Menu drawers;
    private Menu menu;
    private DrawerLayout drawer;
    private static User currentUser;
    private String lastFragment = "HOME";
    private TextView nav_user, nav_email;
    private MenuItem nav_shop, nav_book, nav_login, btnMenuSearch, nav_profil, nav_reserve;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private ActionBarDrawerToggle toggle;
    private FragmentTransaction currentFragment;
    private LinearLayout mainLayout;
    private InterfaceServeur serveur;
    private Call<List<Book>> call;


    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.ACCESS_NETWORK_STATE
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        serveur = RetrofitInstance.getInstance().create(InterfaceServeur.class);


        //TROUVE LES VIEWS.
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);
        drawers = navigationView.getMenu();

        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        nav_user = header.findViewById(R.id.nav_username);
        nav_email = header.findViewById(R.id.nav_email);
        nav_shop = drawers.findItem(R.id.nav_shop);
        nav_login = drawers.findItem(R.id.nav_connection);
        nav_book = drawers.findItem(R.id.nav_book);
        nav_reserve =drawers.findItem(R.id.nav_reserve);
        nav_profil= drawers.findItem(R.id.nav_profil);
        mainLayout = findViewById(R.id.mainLayout);

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                DataSingleton data = DataSingleton.getInstance();

                if(data.getConnectedUser() != null){
                    nav_user.setText(data.getConnectedUser().getFirstName());
                    nav_email.setText(data.getConnectedUser().getEmail());
                    nav_login.setTitle("Deconnexion");
                    nav_reserve.setVisible(true);
                    nav_book.setVisible(true);
                    nav_profil.setVisible(true);
                }else
                {
                    nav_user.setText("");
                    nav_email.setText("");
                    nav_login.setTitle("Connexion");
                    nav_reserve.setVisible(false);
                    nav_book.setVisible(false);
                    nav_profil.setVisible(false);

                }
            }
        });



        //DÉCLARATIONS DES VARIABLES.
        setSupportActionBar(toolbar);
        intent = getIntent();
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_draw_open, R.string.navigation_draw_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        context = this;

        //APPEL LE FRAGMENT "SEARCH" LORS DU LANCEMENT DE L'APPLICATION.
        if (savedInstanceState == null) {
            currentFragment = getSupportFragmentManager().beginTransaction();
            currentFragment.replace(R.id.fragment_container, new HomeFragment(), "HOME").commit();
            nav_shop.setChecked(true);
        }

        //RÉCUPÈRE LE DERNIER FRAGMENT UTILISÉ SI ON A CHANGÉ DE LA MAINACTIVITY (RETOUR AU FRAGMENT AVANT LE LANCEMENT D'UNE AUTRE ACTIVITÉ).
        if (intent.getExtras() != null) {
            lastFragment = intent.getExtras().getString("fragment", "HOME");
        }

        //VÉRIFIE LE DERNIER FRAGMENT UTILISÉ ET LE RELANCE.
        if (lastFragment.equals("SHOP")) {
            toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            mainLayout.setBackground(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
            currentFragment = getSupportFragmentManager().beginTransaction();
            currentFragment.replace(R.id.fragment_container, new ShopFragment(), "SHOP").commit();
            nav_shop.setChecked(true);
        } else if (lastFragment.equals("BOOK")) {
            toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            mainLayout.setBackground(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
            currentFragment = getSupportFragmentManager().beginTransaction();
            currentFragment.replace(R.id.fragment_container, new BookFragment(), "BOOK").commit();
            nav_book.setChecked(true);
        }else if (lastFragment.equals("RESERVATION")) {
            toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            mainLayout.setBackground(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
            currentFragment = getSupportFragmentManager().beginTransaction();
            currentFragment.replace(R.id.fragment_container, new ReservationFragment(), "RESERVATION").commit();
            nav_book.setChecked(true);
        }


        //VÉRIFIE SI L'UTILISATEUR DONNE LES PERMISSIONS
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

    }

    //FONCTION PERMETTANT DE VÉRIFIER SI ON A TOUS LES PERMISSIONS.
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    //FONCTION APPELÉ LORS DE LA CRÉATION DU MENU.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.shop_menu, menu);
        this.menu = menu;

        btnMenuSearch = menu.findItem(R.id.btnMenuSearch);

        if (lastFragment.equals("SHOP")) {
            btnMenuSearch.setVisible(true);
        } else {
            btnMenuSearch.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    //FONCTION APPELÉE LORSQU'UN BOUTON DANS LE MENU EST SELECTIONNÉ.
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {

            case R.id.btnMenuSearch:
                toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorTransparent)));
                mainLayout.setBackground(getDrawable(R.drawable.background));
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                if (btnMenuSearch != null) {
                    btnMenuSearch.setVisible(false);
                }

                ft.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_out_down);
                ft.replace(R.id.fragment_container, new HomeFragment(), "HOME");
                ft.commit();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //FONCTION SERVANT À NAVIGUER DANS LE MENU HAMBURGER.
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Intent intent;

        switch (menuItem.getItemId()) {
            case R.id.nav_shop:
                toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorTransparent)));
                mainLayout.setBackground(getDrawable(R.drawable.background));
                currentFragment = getSupportFragmentManager().beginTransaction();
                currentFragment.replace(R.id.fragment_container, new HomeFragment(), "HOME").commit();
                btnMenuSearch.setVisible(false);
                nav_shop.setChecked(true);
                break;
            case R.id.nav_book:
                toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                mainLayout.setBackground(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
                currentFragment = getSupportFragmentManager().beginTransaction();
                currentFragment.replace(R.id.fragment_container, new BookFragment(), "BOOK").commit();
                btnMenuSearch.setVisible(false);
                nav_book.setChecked(true);
                break;
            case R.id.nav_reserve:
                toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                mainLayout.setBackground(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
                currentFragment = getSupportFragmentManager().beginTransaction();
                currentFragment.replace(R.id.fragment_container, new ReservationFragment(), "RESERVATION").commit();
                //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListFragment(), "LIST").commit();
                break;
            case R.id.nav_profil:
                //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListFragment(), "LIST").commit();
                Intent intentProfil = new Intent (context, ProfilActivity.class);
                startActivity(intentProfil);

                break;
            case R.id.nav_about:
                intent = new Intent(context, About.class);
                startActivity(intent);
                break;
            case R.id.nav_connection:
                DataSingleton data = DataSingleton.getInstance();

                if (data.getConnectedUser() == null) {

                    intent = new Intent(context, LoginActivity.class);
                    startActivity(intent);

                   /* getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LoginFragment(), "LOGIN").commit();*/
                    nav_shop.setChecked(false);
                    } else {

                    Toast.makeText(this, "Déconnnexion", Toast.LENGTH_SHORT).show();

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment(), "HOME").commit();
                    nav_shop.setChecked(true);
                    data.setConnectedUser(null);
                }
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    public void searchBook() {

    }

    //CALLBACK DE L'INTERFACE SEARCHBOOK SERVANT À FAIRE LA RECHERCHE DES LIVRES
    @Override
    public void searchBook(String title) {

        call = serveur.search_book(title);

        call.enqueue(new Callback<List<Book>>() {
            @Override
            public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
                ShopFragment shopFragment = new ShopFragment();
                Bundle data = new Bundle();

                data.putSerializable("bookArray", (Serializable) response.body());
                shopFragment.setArguments(data);

                toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                mainLayout.setBackground(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
                currentFragment = getSupportFragmentManager().beginTransaction();
                btnMenuSearch.setVisible(true);

                currentFragment.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
                currentFragment.replace(R.id.fragment_container, shopFragment, "SHOP");
                currentFragment.commit();
            }

            @Override
            public void onFailure(Call<List<Book>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Caractère non valide.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void advancedSearch() {
        toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        mainLayout.setBackground(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
        currentFragment = getSupportFragmentManager().beginTransaction();

        currentFragment.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
        currentFragment.replace(R.id.fragment_container, new SearchBook(), "SEARCH");
        currentFragment.commit();

    }

}
