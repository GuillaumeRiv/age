package com.example.bibliothqueage;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Add_concession extends AppCompatActivity {
    //VARIABLES GLOBALES.
    private Intent intent;
    private Book book;
    private CodeScanner mCodeScanner;
    private Activity activity;
    private EditText etCodeBarreResult;
    private Button btnScannerSearch;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        intent = getIntent();
        activity = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_concession);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        builder = new AlertDialog.Builder(this);


        //TROUVE LES VIEWS.
        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        etCodeBarreResult = findViewById(R.id.etCodeBarreResult);
        btnScannerSearch = findViewById(R.id.btnScannerSearch);


        //AJOUTE LE ALERT DIALOG LISTENER.
        final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        Intent myIntent = new Intent(getApplicationContext(), Create_book.class);
                        myIntent.putExtra("barcode", etCodeBarreResult.getText().toString());
                        startActivity(myIntent);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };


        //AJOUTE LE SCANNER
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {

            @Override
            public void onDecoded(@NonNull final com.google.zxing.Result result) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        etCodeBarreResult.setText(result.getText());

                        //REÇOIS LE LIVRE DE LA DATABASE.
                        InterfaceServeur serveur = RetrofitInstance.getInstance().create(InterfaceServeur.class);
                        Call<Book> call = serveur.getBookByBarcode(etCodeBarreResult.getText().toString());

                        call.enqueue(new Callback<Book>() {
                            @Override
                            public void onResponse(Call<Book> call, Response<Book> response) {
                                book = response.body();

                                Intent myIntent = new Intent(getApplicationContext(), Add_concession_details.class);
                                myIntent.putExtra("Book", book);

                                startActivity(myIntent);
                            }

                            @Override
                            public void onFailure(Call<Book> call, Throwable t) {
                                builder.setMessage("Livre non répertorié, voulez-vous l'ajouter manuellement?").setPositiveButton("Oui", dialogClickListener)
                                        .setNegativeButton("Non", dialogClickListener).show();
                            }
                        });
                    }
                });
            }
        });

        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });


        //BOUTON RECHERCHER
        btnScannerSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //REÇOIS LE LIVRE DE LA DATABASE.
                InterfaceServeur serveur = RetrofitInstance.getInstance().create(InterfaceServeur.class);
                Call<Book> call = serveur.getBookByBarcode(etCodeBarreResult.getText().toString());

                call.enqueue(new Callback<Book>() {
                    @Override
                    public void onResponse(Call<Book> call, Response<Book> response) {
                        book = response.body();

                        Intent myIntent = new Intent(getApplicationContext(), Add_concession_details.class);
                        myIntent.putExtra("Book", book);

                        startActivityForResult(myIntent, 0);
                    }

                    @Override
                    public void onFailure(Call<Book> call, Throwable t) {
                        builder.setMessage("Livre non répertorié, voulez-vous l'ajouter manuellement?").setPositiveButton("Oui", dialogClickListener)
                                .setNegativeButton("Non", dialogClickListener).show();
                    }
                });
            }
        });

    }


    //FONTION POUR LES MENUS ITEMS.
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }



        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }


}
