package com.example.bibliothqueage;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    DataSingleton data = DataSingleton.getInstance();
    private InterfaceServeur serveur = RetrofitInstance.getInstance().create(InterfaceServeur.class);

    String username = "";
    String password = "";

    Button bt_login;
    Button bt_create_Acc;
    TextInputEditText et_email;
    TextInputEditText et_password;
    String loginEmail;
    String loginPassword;
    TextView tv_pass_fogot;
    private String m_Text = "aaaaaaaaa";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bt_login = this.findViewById(R.id.btLogin);
        et_email = this.findViewById(R.id.text_email);
        et_password = this.findViewById(R.id.text_password);
        bt_create_Acc = this.findViewById(R.id.btCrAcc);

        tv_pass_fogot = this.findViewById(R.id.tvForgottenPassword);


        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginEmail = et_email.getText().toString();
                loginPassword = et_password.getText().toString();

                if (!(loginEmail == null) && !loginEmail.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
                    Toast.makeText(LoginActivity.this, "Courriel invalide.", Toast.LENGTH_SHORT).show();
                } else {


                    Call<User> call = serveur.get_user_check(loginEmail,loginPassword);

                    call.enqueue(new Callback<User>()
                    {
                        @Override
                        public void onResponse( Call<User> call, Response<User> response) {
                            data.setConnectedUser(response.body());
                            Toast.makeText(getApplicationContext(), "yZEUR" ,Toast.LENGTH_SHORT).show();

                            try{
                                Log.d("Data--Login", response.body().toString());
                            }catch (Exception e){
                                Log.d("Data--Login", e.getMessage());
                            }
                            finish();
                        }
                        @Override
                        public void onFailure(Call<User> call, java.lang.Throwable t) {
                            Log.d("Data--Login", t.getMessage());
                            Toast.makeText(getApplicationContext(), "Les informations saisies ne correspondent à aucun compte." ,Toast.LENGTH_SHORT).show();
                        }
                    });



                }

            }
        });

        bt_create_Acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, LoginCrAccActivity.class);
                startActivity(intent);
            }
        });

        tv_pass_fogot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Saisir votre adresse courriel");

                final EditText input = new EditText(LoginActivity.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // ICI ON METTRA LE CODE POUR ENVOYER UNE REQUETE JSON AU PHP
                        m_Text = input.getText().toString();
                    }
                });
                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();


            }
        });

    }

    //FONTION POUR LES MENUS ITEMS.
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivityForResult(myIntent, 0);
                this.overridePendingTransition(R.anim.animation_enter,
                        R.anim.animation_leave);
                break;
        }


        return true;
    }
}