package com.example.bibliothqueage;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import com.example.bibliothqueage.fragments.ModifyPasswordFragment;
import com.example.bibliothqueage.fragments.ShopFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilActivity extends AppCompatActivity {

    AlertDialog dialog;
    AlertDialog.Builder builder;
    String nom, prenom, email, phone = "";
    private InterfaceServeur serveur = RetrofitInstance.getInstance().create(InterfaceServeur.class);

    TextInputEditText tv_nom, tv_prenom, tv_mail, tv_phone;
    ;
    Button bt_modProfile, bt_modPassword;
    private DataSingleton data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tv_nom = findViewById(R.id.tvProfilNom);
        tv_prenom = findViewById(R.id.tvProfilPrenom);
        tv_mail = findViewById(R.id.tvProfilMail);
        tv_phone = findViewById(R.id.tvProfilPhone);
        bt_modProfile = findViewById(R.id.btProfileMod);
        bt_modPassword = findViewById(R.id.btProfilePassMod);


        data = DataSingleton.getInstance();
        tv_nom.setText(data.getConnectedUser().getLastName());
        tv_prenom.setText(data.getConnectedUser().getFirstName());
        tv_mail.setText(data.getConnectedUser().getEmail());
        tv_phone.setText(data.getConnectedUser().getPhoneNumber());

        bt_modProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nom = tv_nom.getText().toString();
                prenom = tv_prenom.getText().toString();
                email = tv_mail.getText().toString();
                phone = tv_phone.getText().toString();

                User userToUpdate = new User();
                userToUpdate.setId(data.getConnectedUser().getId());
                userToUpdate.setEmail(email);
                userToUpdate.setFirstName(prenom);
                userToUpdate.setLastName(nom);
                userToUpdate.setPhoneNumber(phone);

                Call<ServerResponse> call = serveur.update_user(userToUpdate);
                call.enqueue(new Callback<ServerResponse>() {
                    @Override
                    public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                        try {
                            Log.d("Data--update_user", response.body().getMessage());
                            Toast.makeText(ProfilActivity.this, "Gucci", Toast.LENGTH_SHORT).show();
                            data.getConnectedUser().setLastName(nom);
                            data.getConnectedUser().setFirstName(prenom);
                            data.getConnectedUser().setEmail(email);
                            data.getConnectedUser().setPhoneNumber(phone);
                        } catch (Exception e) {
                            Toast.makeText(ProfilActivity.this, "Fouk", Toast.LENGTH_SHORT).show();

                            Log.d("Data--update_user", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<ServerResponse> call, java.lang.Throwable t) {
                        Toast.makeText(ProfilActivity.this, "Fouk", Toast.LENGTH_SHORT).show();

                        try {
                            Log.d("Data--update_user", t.getMessage());
                        } catch (Exception e) {
                            Log.d("Data--update_user", e.getMessage());
                        }
                    }
                });
                Toast.makeText(ProfilActivity.this, nom + " " + prenom + " " + email + " " + phone, Toast.LENGTH_SHORT).show();

            }
        });


        bt_modPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Button btSavePassword;
                final TextInputEditText old_pass;
                final TextInputEditText new_pass;
                final TextInputEditText new_pass_repeat;


                final LayoutInflater inflaterConnection = getLayoutInflater();
                final View dialogView = inflaterConnection.inflate(R.layout.modify_password, null);
                builder = new AlertDialog.Builder(ProfilActivity.this);
                builder.setTitle("Modifiez votre mot de passe");

                builder.setView(dialogView);
                dialog = builder.create();
                dialog.show();

                btSavePassword = dialogView.findViewById(R.id.bt_profil_save_pass);
                old_pass = dialogView.findViewById(R.id.layout_Oldpassword);
                new_pass = dialogView.findViewById(R.id.layout_newPassword);
                new_pass_repeat = dialogView.findViewById(R.id.layout_repeatPassword);

                old_pass.setText(data.getConnectedUser().getPassword());


                btSavePassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (old_pass.getText().toString().isEmpty() || new_pass.getText().toString().isEmpty() || new_pass_repeat.getText().toString().isEmpty()) {
                            Toast.makeText(ProfilActivity.this, "Veuillez saisir tous les champs.", Toast.LENGTH_SHORT).show();
                        } else if (!new_pass.getText().toString().equals(new_pass_repeat.getText().toString())) {
                            Toast.makeText(ProfilActivity.this, "Les mot de passe ne correspondent pas.", Toast.LENGTH_SHORT).show();
                        } else if (!isValidPassword(new_pass.getText().toString())) {
                            Toast.makeText(ProfilActivity.this, "Le mot de passe n'est pas dans un format valide.", Toast.LENGTH_SHORT).show();
                        }else
                        {
                            Call<ServerResponse> call = serveur.check_password(data.getConnectedUser().getId(),old_pass.getText().toString(),new_pass.getText().toString());
                            call.enqueue(new Callback<ServerResponse>() {
                                @Override
                                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                                    Boolean valid;
                                    try {
                                        Log.d("Data--update_user", response.body().getMessage());
                                        valid = response.body().getSucces();
                                        Toast.makeText(ProfilActivity.this, "Gucci", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(ProfilActivity.this, "Fouk", Toast.LENGTH_SHORT).show();
                                        Log.d("Data--update_user", e.getMessage());
                                        valid =false;
                                    }

                                    if(valid){
                                        Toast.makeText(ProfilActivity.this, "Le password va changer :)", Toast.LENGTH_SHORT).show();
                                    }


                                }
                                @Override
                                public void onFailure(Call<ServerResponse> call, java.lang.Throwable t) {
                                    Toast.makeText(ProfilActivity.this, "Fouk", Toast.LENGTH_SHORT).show();
                                    try {
                                        Log.d("Data--update_user", t.getMessage());
                                    } catch (Exception e) {
                                        Log.d("Data--update_user", e.getMessage());
                                    }
                                }
                            });
                        }
                    }
                });

            }
        });


    }


    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$");

        return PASSWORD_PATTERN.matcher(s).matches();
    }

    //FONTION POUR LES MENUS ITEMS.
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivityForResult(myIntent, 0);
                this.overridePendingTransition(R.anim.animation_enter,
                        R.anim.animation_leave);
                break;
        }


        return true;
    }

}
