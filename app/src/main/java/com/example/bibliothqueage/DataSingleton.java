package com.example.bibliothqueage;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataSingleton {
    private static final DataSingleton ourInstance = new DataSingleton();

    public static DataSingleton getInstance() {
        return ourInstance;
    }

    private DataSingleton() {
    }
    private InterfaceServeur serveur = RetrofitInstance.getInstance().create(InterfaceServeur.class);

    private User connectedUser = null;

    public User getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(User connectedUser) {
        this.connectedUser = connectedUser;
    }

    public void sendUserInfo(User connectedUser, boolean isNew) {

        if(isNew) {
            Call<ServerResponse> call = serveur.add_user(connectedUser);
            call.enqueue(new Callback<ServerResponse>()
            {
                @Override
                public void onResponse( Call<ServerResponse> call, Response<ServerResponse> response) {
                    try{
                        Log.d("Data--add_user", response.body().getMessage());
                    }catch (Exception e){
                        Log.d("Data--add_user", e.getMessage());
                    }
                }
                @Override
                public void onFailure(Call<ServerResponse> call, java.lang.Throwable t) {
                    try{
                        Log.d("Data--add_user", t.getMessage());
                    }catch (Exception e){
                        Log.d("Data--add_user", e.getMessage());
                    }
                }
            });
        }
        else{
            Call<ServerResponse> call = serveur.update_user(connectedUser);
            call.enqueue(new Callback<ServerResponse>()
            {
                @Override
                public void onResponse( Call<ServerResponse> call, Response<ServerResponse> response) {
                    try{
                        Log.d("Data--update_user", response.body().getMessage());
                    }catch (Exception e){
                        Log.d("Data--update_user", e.getMessage());
                    }
                }
                @Override
                public void onFailure(Call<ServerResponse> call, java.lang.Throwable t) {
                    try{
                        Log.d("Data--update_user", t.getMessage());
                    }catch (Exception e){
                        Log.d("Data--update_user", e.getMessage());
                    }
                }
            });
        }
    }

}
