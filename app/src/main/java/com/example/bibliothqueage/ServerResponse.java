package com.example.bibliothqueage;


import com.google.gson.annotations.SerializedName;

public class ServerResponse {

    @SerializedName("succes")
    private Boolean succes;

    @SerializedName("message")
    private String message;

    public Boolean getSucces() {
        return succes;
    }

    public void setSucces(Boolean succes) {
        this.succes = succes;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
