package com.example.bibliothqueage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import com.example.bibliothqueage.fragments.ShopFragment;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.angmarch.views.NiceSpinner;
import org.angmarch.views.OnSpinnerItemSelectedListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Book_detailsV2 extends AppCompatActivity {
    //VARIABLES GLOBALES.
    private Intent intent;
    private Book theBook;
    private TextView tvSummary, tvEdition, tvAuthor, tvPrice, tvPublisher, tvBarcode, tvTitle;
    private NiceSpinner concession_spinner;
    private String currentFragment;
    private int id_book;
    private List<Concession> lstConcession;
    private FloatingActionButton btnReserve;
    private MenuItem btnMenuModifier, btnMenuSupprimer;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private InterfaceServeur serveur;
    private Call<Book> call;
    private Call<List<Concession>> call2;
    private Concession selected_concession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        lstConcession = null;
        intent = getIntent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details_v2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        serveur = RetrofitInstance.getInstance().create(InterfaceServeur.class);

        //TROUVE LES VIEWS.
        tvAuthor = findViewById(R.id.bookAuthor);
        tvEdition = findViewById(R.id.bookEdition);
        tvPrice = findViewById(R.id.bookPrice);
        tvPublisher = findViewById(R.id.bookPublisher);
        tvBarcode = findViewById(R.id.bookBarcode);
        tvSummary = findViewById(R.id.bookSummary);
        btnReserve = findViewById(R.id.btnReserve);
        tvTitle = findViewById(R.id.bookTitle);
        collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        concession_spinner = findViewById(R.id.concession_spinner);


        concession_spinner.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener() {
            @Override
            public void onItemSelected(NiceSpinner parent, View view, int position, long id) {
                String imageURI = "http://206.167.140.56:8080/420617RI/Equipe_3/BiblioAGE/book_imgs/" + lstConcession.get(position).getThumbnail();
                Picasso.with(getApplicationContext()).invalidate(imageURI);
                Picasso.with(getApplicationContext()).load(imageURI).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE);
                selected_concession = lstConcession.get(position);

                Picasso.with(getApplicationContext()).load(imageURI).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        collapsingToolbarLayout.setBackground(new BitmapDrawable(bitmap));
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

                tvPrice.setText(String.format("%.2f", lstConcession.get(position).getSellingPrice()) + "$");
            }
        });

        //RAMASSE LES EXTRAS ENVOYÉS DANS L'INTENT.
        id_book = intent.getExtras().getInt("bookID");
        currentFragment = intent.getExtras().getString("fragment");

        call = serveur.getBookByID(id_book);

        call.enqueue(new Callback<Book>() {
            @Override
            public void onResponse(Call<Book> call, Response<Book> response) {
                theBook = response.body();
                //ENTRE LES INFORMATIONS DU LIVRE DANS LES CHAMPS.
                collapsingToolbarLayout.setTitle(theBook.getTitle());
                tvTitle.setText(theBook.getTitle());
                tvEdition.setText(theBook.getEdition());
                tvAuthor.setText("De " + theBook.getAuthor());
                tvPublisher.setText(theBook.getPublisher());
                tvBarcode.setText(theBook.getBarcode());
                tvSummary.setText(theBook.getSummary());


                call2 = serveur.getConcessionByBook(id_book);

                call2.enqueue(new Callback<List<Concession>>() {
                    @Override
                    public void onResponse(Call<List<Concession>> call, Response<List<Concession>> response) {
                        String imageURI = "http://206.167.140.56:8080/420617RI/Equipe_3/BiblioAGE/book_imgs/default_img.png";
                        lstConcession = response.body();
                        List<String> arrConcession = new ArrayList<String>();
                        Picasso.Builder builder = new Picasso.Builder(getApplicationContext());
                        Picasso.with(getApplicationContext()).invalidate(imageURI);
                        Picasso.with(getApplicationContext()).load(imageURI).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE);

                        if (lstConcession.size() != 0) {

                            Collections.sort(lstConcession, new Comparator<Concession>() {
                                @Override
                                public int compare(Concession o1, Concession o2) {
                                    return Double.compare(o1.getSellingPrice(), o2.getSellingPrice());
                                }
                            });

                            if (lstConcession.size() == 1) {
                                concession_spinner.setText("#" + lstConcession.get(0).getId() + "  -  " + String.format("%.2f", lstConcession.get(0).getSellingPrice()) + "$");
                            } else {
                                for (Concession concession : lstConcession) {
                                    arrConcession.add("#" + concession.getId() + "  -  " + String.format("%.2f", concession.getSellingPrice()) + "$");
                                }
                            }

                            imageURI = "http://206.167.140.56:8080/420617RI/Equipe_3/BiblioAGE/book_imgs/" + lstConcession.get(0).getThumbnail();
                            tvPrice.setText(String.format("%.2f", lstConcession.get(0).getSellingPrice()) + "$");
                            concession_spinner.attachDataSource(arrConcession);
                            selected_concession = lstConcession.get(0);
                        } else {
                            tvPrice.setText("N/A");
                            concession_spinner.setText("Aucun livre en vente");
                        }

                        builder.listener(new Picasso.Listener()
                        {
                            @Override
                            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                                collapsingToolbarLayout.setBackgroundResource(R.drawable.default_img);
                            }

                        });

                        builder.build().load(imageURI).into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                collapsingToolbarLayout.setBackground(new BitmapDrawable(bitmap));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<List<Concession>> call, Throwable t) {

                    }
                });

            }

            @Override
            public void onFailure(Call<Book> call, Throwable t) {

            }
        });

        //VÉRIFIE DANS QUEL FRAGMENT ON CE SITUE ET AJOUTE LES OPTIONS SELON LE FRAGMENT.
        if (currentFragment.equals("SHOP")) {
            btnReserve.show();

            btnReserve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });
        } else {
            btnReserve.hide();
        }

    }

    //FONCTION APPELÉ LORS DE LA CRÉATION DU MENU.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details_menu, menu);

        btnMenuModifier = menu.findItem(R.id.btnMenuModifier);
        btnMenuSupprimer = menu.findItem(R.id.btnMenuSupprimer);

        if (currentFragment.equals("SHOP")) {
            btnMenuSupprimer.setVisible(false);
            btnMenuModifier.setVisible(false);
        } else if (currentFragment.equals("BOOK")) {
            btnMenuSupprimer.setVisible(true);
            btnMenuModifier.setVisible(true);
        }

        return true;
    }

    //FONTION POUR LES MENUS ITEMS.
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.btnMenuModifier:
                /*if (theBook.getIsReserved() == 0) {
                    Intent intent = new Intent(this, Modify_book.class);
                    intent.putExtra("bookID", theBook.getId());
                    startActivityForResult(intent, 2);
                } else {
                    final AlertDialog.Builder builder=new AlertDialog.Builder(this);
                    builder.setCancelable(false);
                    builder.setTitle("Attention");
                    builder.setMessage("Ce livre est déjà acheté!");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.show();
                }*/
                break;


            case R.id.btnMenuSupprimer:

                break;

        }

        return true;
    }


    //REÇOIS LES INFORMATIONS LORS DE LA MODIFICATION D'UN LIVRE
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {




            }
        }
    }
}
