package com.example.bibliothqueage;

import android.icu.text.SimpleDateFormat;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Book implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("author")
    private String author;

    @SerializedName("publisher")
    private String publisher;

    @SerializedName("edition")
    private String edition;

    @SerializedName("summary")
    private String summary;

    @SerializedName("createdBy")
    private int createdBy;

    @SerializedName("barcode")
    private String barcode;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("originalCodeBarres")
    private String originalCodeBarres;

    public Book() {

    }

    public Book(int id, String title, String author, String publisher, String edition, String summary, int createdBy, String barcode, String thumbnail, String originalCodeBarres) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.edition = edition;
        this.summary = summary;
        this.createdBy = createdBy;
        this.barcode = barcode;
        this.thumbnail = thumbnail;
        this.originalCodeBarres = originalCodeBarres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getOriginalCodeBarres() {
        return originalCodeBarres;
    }

    public void setOriginalCodeBarres(String originalCodeBarres) {
        this.originalCodeBarres = originalCodeBarres;
    }
}
