package com.example.bibliothqueage;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class Book_details extends AppCompatActivity {
    //VARIABLES GLOBALES.
    private Intent intent;
    private Book theBook;
    private TextView tvTitle, tvSummary, tvEdition, tvAuthor, tvPrice, tvPublisher, tvBarcode;
    private ImageView img;
    private String currentFragment;
    //private User current_user;
    private int id_book;
    private FloatingActionButton btnReserve;
    private MenuItem btnMenuModifier, btnMenuSupprimer;

    //**TEST**
    private List<Book> lstBook = new ArrayList<>();
    //**TEST**

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        intent = getIntent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //current_user = MainActivity.getCurrent_user();


        // **TEST**
        //lstBook.add(new Book(1, "Martine en avion", "Gilbert Delahaye", "Casterman", "2005", "Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...", 1, "1324", "12314"));
        //lstBook.add(new Book(1, "Martine en bateau", "Gilbert Delahaye", "Casterman", "2005", "Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...", 1, "1324", "12314"));
        // **TEST**


        //TROUVE LES VIEWS.
        img = findViewById(R.id.bookImage_id);
        tvTitle = findViewById(R.id.bookTitle_id);
        tvAuthor = findViewById(R.id.bookAuthor);
        tvEdition = findViewById(R.id.bookEdition_id);
        tvPrice = findViewById(R.id.bookPrice);
        tvPublisher = findViewById(R.id.bookPublisher);
        tvBarcode = findViewById(R.id.bookBarcode);
        tvSummary = findViewById(R.id.bookSummary);
        btnReserve = findViewById(R.id.btnReserve);

        //RAMASSE LES EXTRAS ENVOYÉS DANS L'INTENT.
        id_book = intent.getExtras().getInt("bookID") - 1;
        theBook = lstBook.get(id_book);
        currentFragment = intent.getExtras().getString("fragment");

        //ENTRE LES INFORMATIONS DU LIVRE DANS LES CHAMPS.
        img.setImageResource(R.drawable.default_img);
        tvTitle.setText(lstBook.get(id_book).getTitle() + " " + lstBook.get(id_book).getEdition());
        tvAuthor.setText("De " + lstBook.get(id_book).getAuthor());
        //tvPrice.setText(String.format("%.2f", lstBook.get(id_book).getPrice()) + "$");
        tvPublisher.setText(lstBook.get(id_book).getPublisher());
        tvBarcode.setText(lstBook.get(id_book).getBarcode());
        tvSummary.setText(lstBook.get(id_book).getSummary());

        //VÉRIFIE DANS QUEL FRAGMENT ON CE SITUE ET AJOUTE LES OPTIONS SELON LE FRAGMENT.
        if (currentFragment.equals("SHOP")) {
            btnReserve.show();

            btnReserve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });
        } else {
            btnReserve.hide();
        }

    }

    //FONCTION APPELÉ LORS DE LA CRÉATION DU MENU.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details_menu, menu);

        btnMenuModifier = menu.findItem(R.id.btnMenuModifier);
        btnMenuSupprimer = menu.findItem(R.id.btnMenuSupprimer);

        if (currentFragment.equals("SHOP")) {
            btnMenuSupprimer.setVisible(false);
            btnMenuModifier.setVisible(false);
        } else if (currentFragment.equals("BOOK")) {
            btnMenuSupprimer.setVisible(true);
            btnMenuModifier.setVisible(true);
        }

        return true;
    }

    //FONTION POUR LES MENUS ITEMS.
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                myIntent.putExtra("fragment", currentFragment);
                startActivityForResult(myIntent, 0);
                this.overridePendingTransition(R.anim.animation_enter,
                        R.anim.animation_leave);
                break;

            case R.id.btnMenuModifier:
                /*if (theBook.getIsReserved() == 0) {
                    Intent intent = new Intent(this, Modify_book.class);
                    intent.putExtra("bookID", theBook.getId());
                    startActivityForResult(intent, 2);
                } else {
                    final AlertDialog.Builder builder=new AlertDialog.Builder(this);
                    builder.setCancelable(false);
                    builder.setTitle("Attention");
                    builder.setMessage("Ce livre est déjà acheté!");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.show();
                }*/
                break;


            case R.id.btnMenuSupprimer:

                break;

        }

        return true;
    }


    //REÇOIS LES INFORMATIONS LORS DE LA MODIFICATION D'UN LIVRE
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {




            }
        }
    }




}
