package com.example.bibliothqueage.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.bibliothqueage.Book;
import com.example.bibliothqueage.MainActivity;
import com.example.bibliothqueage.R;
import com.example.bibliothqueage.RecyclerViewAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ShopFragment extends Fragment {
    //VARIABLES GLOBALES.
    private FloatingActionButton btnRefreshShop;
    private List<Book> lstBook;

    //**TEST**
    //public static List<Book> lstBook = new ArrayList<>();
    //**TEST**

    //CONSTRUCTEUR POUR LE FRAGMENT.
    public ShopFragment() {

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop, container, false);
        lstBook = (List<Book>) getArguments().getSerializable("bookArray");


        //TROUVE LES VIEWS.
        btnRefreshShop = view.findViewById(R.id.btnRefreshShop);


        //**TEST**
        //lstBook.add(new Book(1, "Martine en avion", "Gilbert Delahaye", "Casterman", "2005", "Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...", 1, "1324", "12314"));
        //lstBook.add(new Book(1, "Martine en bateau", "Gilbert Delahaye", "Casterman", "2005", "Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...", 1, "1324", "12314"));
        //**TEST**



        //BOUTON SERVANT À REFRESH LA SECTION "SHOP".
        btnRefreshShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        //DÉMARRE LE RECYCLERVIEW ET SET LE CURRENT FRAGMENT DANS L'ADAPTER.
        RecyclerView myrv = view.findViewById(R.id.recyclerview_id);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getActivity(), lstBook);
        adapter.setCurrentFragment("SHOP");
        myrv.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        myrv.setAdapter(adapter);

        return view;
    }

}
