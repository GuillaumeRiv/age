package com.example.bibliothqueage.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.bibliothqueage.MainActivity;
import com.example.bibliothqueage.R;

public class SearchBook extends Fragment {
    //INTERFACE POUR LE CALLBACK.
    public interface searchInterface {
        void searchBook();
    }

    //VARIABLES GLOBALES.
    private SeekBar sbSearchPrice;
    private EditText etSearchTitre, etSearchEdition, etSearchAuthor, etSearchPublishor;
    private TextView tvSearchMaxPrice;
    private Button btnSearchBook;
    private int searchMaxPrice;
    private searchInterface mInterface;

    //CONSTRUCTEUR POUR LE FRAGMENT.
    public SearchBook() {

    }


    //ATTACHE LE CONTEXT À L'INTERFACE.
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        mInterface = (searchInterface) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_book, container, false);


        //TROUVE LES VIEWS.
        sbSearchPrice = view.findViewById(R.id.sbSearchPrice);
        tvSearchMaxPrice = view.findViewById(R.id.tvSearchMaxPrice);
        etSearchTitre = view.findViewById(R.id.etSearchTitre);
        etSearchEdition = view.findViewById(R.id.etSearchEdition);
        etSearchAuthor = view.findViewById(R.id.etSearchAuthor);
        etSearchPublishor = view.findViewById(R.id.etSearchPublishor);
        btnSearchBook = view.findViewById(R.id.btnSearchBook);


        //AJUSTE LA VALEUR DU SEEKBAR LORSQUE CHANGÉ.
        sbSearchPrice.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                searchMaxPrice = progress + 25;
                searchMaxPrice = searchMaxPrice / 25;
                searchMaxPrice = searchMaxPrice * 25;
                tvSearchMaxPrice.setText("0$ - " + searchMaxPrice + "$");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //BOUTON SERVANT À FAIRE LA REHERCHE (CALLBACK).
        btnSearchBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterface.searchBook();
            }
        });


        return view;
    }

}
