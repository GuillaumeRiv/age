package com.example.bibliothqueage.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.bibliothqueage.Add_concession;
import com.example.bibliothqueage.Book;
import com.example.bibliothqueage.Book_details;
import com.example.bibliothqueage.MainActivity;
import com.example.bibliothqueage.R;
import com.example.bibliothqueage.RecyclerViewAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReservationFragment extends Fragment {
    //VARIABLES GLOBALES.



    //**TEST**
    public static List<Book> lstBookReservation = new ArrayList<>();
    //**TEST**

    //CONSTRUCTEUR POUR LE FRAGMENT.
    public ReservationFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reservation, container, false);


        //**TEST**
        //lstBookReservation.add(new Book(1, "Martine en avion", "Gilbert Delahaye", "Casterman", "2005", "Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...", 1, "1324", "12314"));
        //lstBookReservation.add(new Book(1, "Martine en bateau", "Gilbert Delahaye", "Casterman", "2005", "Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...Summary ici...", 1, "1324", "12314"));
       //**TEST**




        //DÉMARRE LE RECYCLERVIEW ET SET LE CURRENT FRAGMENT DANS L'ADAPTER.
        RecyclerView myrv = view.findViewById(R.id.recyclerview_id);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getActivity(), lstBookReservation);
        adapter.setCurrentFragment("RESERVATION");
        myrv.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        myrv.setAdapter(adapter);


        return view;
    }

}
