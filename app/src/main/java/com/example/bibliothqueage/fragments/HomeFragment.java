package com.example.bibliothqueage.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.bibliothqueage.R;

public class HomeFragment extends Fragment {
    //INTERFACE POUR LE CALLBACK.
    public interface homeInterface {
        void searchBook(String title);
        void advancedSearch();
    }

    //VARIABLES GLOBALES.
    private Button btnHomeSearch, btnHomeAdvancedSearch;
    private EditText etHomeSearch;
    private homeInterface mInterface;


    //CONSTRUCTEUR POUR LE FRAGMENT.
    public HomeFragment() {

    }

    //ATTACHE LE CONTEXT À L'INTERFACE.
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        mInterface = (homeInterface) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);


        //TROUVE LES VIEWS.
        btnHomeSearch = view.findViewById(R.id.btnHomeSearch);
        btnHomeAdvancedSearch = view.findViewById(R.id.btnHomeAdvancedSearch);
        etHomeSearch = view.findViewById(R.id.etHomeSearch);

        //AJOUTE LES ONCLICK DES BOUTONS.
        btnHomeSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterface.searchBook(etHomeSearch.getText().toString());
            }
        });

        btnHomeAdvancedSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mInterface.advancedSearch();
            }
        });


        return view;
    }

}
